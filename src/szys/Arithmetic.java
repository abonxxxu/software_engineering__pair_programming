package szys;

import java.util.Random;

public class Arithmetic {
	int a = new Random().nextInt(5);
	int b = new Random().nextInt(10)%(10-1+1) + 1;
	int c = new Random().nextInt(10)%(10-2+1) + 2;
	int d = new Random().nextInt(10)%(10-1+1) + 1;
	int e = new Random().nextInt(10)%(10-2+1) + 2;
	int f = new Random().nextInt(100);
	int g = new Random().nextInt(100);
	String astr="";
	String qstr="";
	boolean x;
	
	int [][] num2 = new int[10][4];
	int [][] num1 = new int[10][2];
	int [] num3 = new int [10];//保存每道题的结果
	int flage = 0;//记录num3数组中的元素个数
	int flage2 = 0;//保存整数数组num1中的个数
	int flage3= 0;//保存分数数组num2的个数
	public Arithmetic(boolean x) {
		this.x=x;
	}
	public String int_operation()
	{
		int result = 0;
		if(a==0)
			result=f+g;
		if(a==1)
			result=f-g;
		if(a==2)
			result=f*g;
		for(int i = 0; i <= flage; i++)//查询num3中有没有与result相同的元素
		{
			if(result == num3[i])
				return null;//如果有相同的返回null
		}
		num3[flage++] = result;//如果没有相同的就把新的result的值放入num3中
		astr = String.valueOf( result);
		if(a==3)
		{
			if(g==0)
			{
				astr=int_operation();
				return astr;
			}
			else
			{
				if(g!=0&&g!=1){
					int d=common_divisor(f,g);
					f=f/d;
					g=g/d;
					astr = (f+"/"+g);
				}
				if(g==1)
					astr=(""+f);
			}
			
		}
		if(a==4)
		{
			result=(int) Math.pow(f, g);
		}
		return astr;
	}
	public String fra_operation(){
		this.b = new Random().nextInt(10)%(10-1+1) + 1;
		this.c = new Random().nextInt(10)%(10-2+1) + 2;
		this.d = new Random().nextInt(10)%(10-1+1) + 1;
		this.e = new Random().nextInt(10)%(10-2+1) + 2;
		if(c<b||e<d||c%b==0||e%d==0)
		{
			astr=fra_operation();
			return astr;
		}
			
		int fz=1,fm=c*e;
		if(a==0)
			fz=b*e+c*d;
		if(a==1){
			fz=b*e-c*d;
			if(fz==0)
			{
				return astr=("0");
			}
		}
			
		if(a==2)
			fz=b*d;
		if(a==3)
		{
			fz=b*e;
			fm=c*d;
		}
		int f=common_divisor(fm,fz);
		if(f>0){
			fm=fm/f;
			fz=fz/f;
		}
		if(f<0){
			fm=-fm/f;
			fz=-fz/f;
		}
		if(a==4)
		{
			fz=(int) Math.pow(b*e, f);
			fm=(int) Math.pow(c*d, f);
			
		}
		astr = (fz+"/"+fm);
		return astr;
		
	}
	public static int common_divisor(int m,int n)
	{
		while(m%n!=0){
			int t=m%n;
			m=n;
			n=t;
		}
		return n;
	}
	public String toString(){
		if(x==true){
			//先判断在num1里面有没有相同的元素
			for(int i = 0; i <= flage2; i++)
			{
				if((f == num1[i][0] && g == num1[i][1]) || (f == num1[i][0] || g == num1[i][1]))
					return null;
			}
			//如果没有就把原来的值放入
			num1[flage2++][0] = f;
			num1[flage][1] = g;
			
			if(a==0)
				qstr=(f+"+"+g+"=");
			if(a==1)
				qstr=(f+"-"+g+"=");
			if(a==2)
				qstr=(f+"*"+g+"=");
			if(a==3)
				qstr=(f+"/"+g+"=");
			if(a==3)
				qstr=(f+"/"+g+"=");
			if(a==4)
				qstr=(f+"^"+g+"=");
		}
		if(x==false){
			//先判断在num2里面有没有分数运算相同的元素
			for(int i = 0; i <= flage3; i++)
			{
				if((b == num2[i][0] && c == num2[i][1] && d == num2[i][2] && e == num2[i][3]) || (b == num2[i][2] && c == num2[i][3] && d == num2[i][0] && e == num2[i][1]))
					return null;
			}
			//如果没有就把原来的值放入
			num2[flage3++][0] = b;
			num2[flage3][1] = c;
			num2[flage3][2] = d;
			num2[flage3][3] = e;
			if(a==0)
				qstr=(b+"/"+c+"+"+d+"/"+e+"=");
			if(a==1)
				qstr=(b+"/"+c+"-"+d+"/"+e+"=");
			if(a==2)
				qstr=(b+"/"+c+"*"+d+"/"+e+"=");
			if(a==3)
				qstr=(b+"/"+c+"/"+d+"/"+e+"=");
			if(a==4)
				qstr=(b+"/"+c+"^"+d+"/"+e+"=");
		}
		return qstr;
	}
	
}
