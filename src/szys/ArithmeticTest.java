package szys;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ArithmeticTest {
	
	private static Arithmetic arithmetic = new Arithmetic(false);
	@Before
	public void setUp() throws Exception {
		arithmetic.clear();
	}

	@Test
	public void testAdd() {
		arithmetic.add("3");
		arithmetic.add("4");
		assertEquals("7",arithmetic.getAstr());
	}

	@Test
	public void testSubstract() {
		arithmetic.add("8");
		arithmetic.substract("5");
		assertEquals("3",arithmetic.getAstr());
	}

	@Test
	public void testMultiply() {
		arithmetic.add("3");
		arithmetic.multiply("4");
		assertEquals("12",arithmetic.getAstr());
	}

	@Test
	public void testDivide() {
		arithmetic.add("8");
		arithmetic.divide("4");
		assertEquals("2",arithmetic.getAstr());
	}

}
